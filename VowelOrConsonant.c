#include <stdio.h>

//This function is for deciding whether a given character is vowel or consonant.
int vowelOrConstant(char letter){
  if(letter=='a'||letter=='A'){
    printf("Input was vowel \n");
  }else if(letter=='e'||letter=='E'){  
    printf("Input was vowel \n");
  }else if(letter=='i'||letter=='I'){  
    printf("Input was vowel \n");
  }else if(letter=='o'||letter=='O'){  
    printf("Input was vowel \n");
  }else if(letter=='u'||letter=='U'){  
    printf("Input was vowel \n");
  }else{
    printf("Input was consonant \n");
  }
}

//In main function user is allowed to input a character and then above function is called.
int main(){
  char inputLetter;
  printf("Input any letter : ");
  inputLetter = getchar();
  vowelOrConstant(inputLetter);
}


